FROM alpine:3.11.3 AS hysop
MAINTAINER Jean-Baptiste.Keck@imag.fr

ARG NTHREADS
ENV MAKEFLAGS "-j${NTHREADS}"

ENV HYSOP_DEPS="/opt/hysop"
ENV BUILD_DEPS="/opt/build"

ENV GCC_ROOT="${HYSOP_DEPS}/opt/gcc"
ENV LLVM_ROOT="${HYSOP_DEPS}/opt/llvm"
ENV MPI_ROOT="${HYSOP_DEPS}/opt/openmpi"
ENV BLAS_ROOT="${HYSOP_DEPS}/opt/blas"
ENV OPENCL_ROOT="${HYSOP_DEPS}/opt/opencl"

ENV CFLAGS="-fPIC"
ENV CXXFLAGS="-fPIC"
ENV CPPFLAGS="-fPIC"

ENV PATH="${HYSOP_DEPS}/bin:${BUILD_DEPS}/bin:${PATH}"
ENV LD_LIBRARY_PATH="${HYSOP_DEPS}/lib:${BUILD_DEPS}/lib:${LD_LIBRARY_PATH}"
ENV PKG_CONFIG_PATH="${HYSOP_DEPS}/lib/pkgconfig:${HYSOP_DEPS}/share/pkgconfig:${BUILD_DEPS}/lib/pkgconfig:${BUILD_DEPS}/share/pkgconfig:${PKG_CONFIG_PATH}"
ENV ACLOCAL_PATH="${HYSOP_DEPS}/share/aclocal:${BUILD_DEPS}/share/aclocal:${ACLOCAL_PATH}"

RUN mkdir -p "${BUILD_DEPS}/bin" && ln -s "${BUILD_DEPS}/bin" "${BUILD_DEPS}/sbin" && \
    mkdir -p "${BUILD_DEPS}/lib" && ln -s "${BUILD_DEPS}/lib" "${BUILD_DEPS}/lib64" && \
    mkdir -p "${HYSOP_DEPS}/bin" && ln -s "${HYSOP_DEPS}/bin" "${HYSOP_DEPS}/sbin" && \
    mkdir -p "${HYSOP_DEPS}/lib" && ln -s "${HYSOP_DEPS}/lib" "${HYSOP_DEPS}/lib64" && \
    mkdir -p "${OPENCL_ROOT}/bin" && mkdir -p "${OPENCL_ROOT}/include" && \
    mkdir -p "${OPENCL_ROOT}/lib" && ln -s "${OPENCL_ROOT}/lib" "${OPENCL_ROOT}/lib64"

RUN apk add --no-cache binutils build-base gfortran linux-headers pkgconfig autoconf automake libtool cmake perl ruby bison flex bsd-compat-headers patchelf lddtree git vim vimdiff openssh ccache

################################
# GCC 9.3.0 and dependencies:
#   GMP 6.1.0
#   MPFR 3.1.4  
#   MPC 1.0.3
#   ISL 0.18
################################

ADD patch/gcc.patch /tmp/gcc.patch
RUN cd /tmp && \
 wget https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.xz && \
 tar -xvJf gcc-9.3.0.tar.xz && \
 patch -p0 -i "/tmp/gcc.patch" && \
 cd gcc-9.3.0 && \
 ./contrib/download_prerequisites --directory /tmp && \
 cd - && \
 rm -f /tmp/gcc-9.3.0.tar.xz && \
 rm -f /tmp/gmp-*.tar.bz2 && \
 rm -f /tmp/mpfr-*.tar.bz2 && \
 rm -f /tmp/mpc-*.tar.gz && \
 rm -f /tmp/isl-*.tar.bz2 && \
 cd gmp-* && \
 ./configure --enable-static=yes --enable-shared=no --prefix="$BUILD_DEPS" && \
 make -j"${NTHREADS}" && \
 make install && \
 cd - && \
 rm -Rf /tmp/gmp* && \
 cd mpfr-* && \
 ./configure --enable-static=yes --enable-shared=no --prefix="$BUILD_DEPS" --with-gmp="${BUILD_DEPS}" && \
 make -j"${NTHREADS}" && \
 make install && \
 cd - && \
 rm -Rf /tmp/mpfr* && \
 cd mpc-* && \
 ./configure --enable-static=yes --enable-shared=no --prefix="$BUILD_DEPS" --with-gmp="${BUILD_DEPS}" --with-mpfr="${BUILD_DEPS}" && \
 make -j"${NTHREADS}" && \
 make install && \
 cd - && \
 rm -Rf /tmp/mpc* && \
 cd isl-* && \
 ./configure --enable-static=yes --enable-shared=no --prefix="$BUILD_DEPS" --with-gmp-prefix="${BUILD_DEPS}" && \
 make -j"${NTHREADS}" && \
 make install && \
 cd - && \
 rm -Rf /tmp/isl* && \
 mkdir gcc-build && \
 cd gcc-build && \
 ../gcc-9.3.0/configure --prefix="${GCC_ROOT}" --with-mpc="${BUILD_DEPS}" --with-mpfr="${BUILD_DEPS}" --with-gmp="${BUILD_DEPS}" --with-isl="${BUILD_DEPS}" --disable-multilib --disable-libsanitizer --enable-languages=c,c++,fortran --enable-bootstrap  && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/gcc*

ENV CC="${GCC_ROOT}/bin/gcc"
ENV CXX="${GCC_ROOT}/bin/g++"
ENV FC="${GCC_ROOT}/bin/gfortran"

###########################################################################################
### PYTHON 2.7 DEPENDENCIES (with everything but tcl/tk because this requires X libraries)
###########################################################################################

# static expat library 2.2.9
RUN cd /tmp && \
 wget https://github.com/libexpat/libexpat/releases/download/R_2_2_9/expat-2.2.9.tar.bz2 && \
 tar -xvjf expat-2.2.9.tar.bz2 && \
 cd expat-2.2.9 && \
 ./configure --prefix="${BUILD_DEPS}" --disable-shared && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/expat*

# static libffi 3.3
RUN cd /tmp && \
 wget https://github.com/libffi/libffi/releases/download/v3.3/libffi-3.3.tar.gz && \
 tar -xvzf libffi-3.3.tar.gz && \
 cd libffi-3.3 && \
 ./configure --prefix="${BUILD_DEPS}" --disable-shared && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/libffi*

# static libz2 1.0.8 (fix CFLAGS to take into account -fPIC)
RUN cd /tmp && \
 wget ftp://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz && \
 tar -xvzf bzip2-1.0.8.tar.gz && \
 cd bzip2-1.0.8 && \
 sed -i 's/CFLAGS=/CFLAGS += /g' Makefile && \
 make -j"${NTHREADS}" && \
 make install PREFIX="${BUILD_DEPS}" && \
 rm -Rf /tmp/bzip2*

# static zlib 1.2.11
RUN cd /tmp && \
 wget https://www.zlib.net/zlib-1.2.11.tar.gz && \
 tar -xvzf zlib-1.2.11.tar.gz && \
 cd zlib-1.2.11 && \
 ./configure --prefix="${BUILD_DEPS}" --static && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/zlib*

# static ncurses 6.1
RUN cd /tmp && \
 wget https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.1.tar.gz && \
 tar -xvzf ncurses-6.1.tar.gz && \
 cd ncurses-6.1 && \
 ./configure --prefix=${BUILD_DEPS} --disable-shared --with-termlib && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/ncurses*

# static openssl 1.0.2u
RUN cd /tmp && \
 wget https://github.com/openssl/openssl/archive/OpenSSL_1_1_1g.tar.gz && \
 tar -xvzf OpenSSL_1_1_1g.tar.gz && \
 cd openssl-OpenSSL_1_1_1g && \
 ./config --prefix="${BUILD_DEPS}" --openssldir=/tmp/openssl threads zlib -no-shared -L"${BUILD_DEPS}/lib" -I"${BUILD_DEPS}/include" -fPIC && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/openssl* && \
 rm -Rf /tmp/OpenSSL*

# static readline 8.0
RUN cd /tmp && \
 wget ftp://ftp.cwru.edu/pub/bash/readline-8.0.tar.gz && \
 tar -xvzf readline-8.0.tar.gz && \
 cd readline-8.0 && \
 ./configure --prefix="${BUILD_DEPS}" --enable-static=yes --enable-shared=no && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/readline*

# static libgdbm and libgdbm-compat 1.18.1
RUN cd /tmp && \
 wget ftp://ftp.gnu.org/gnu/gdbm/gdbm-1.18.1.tar.gz && \
 tar -xvzf gdbm-1.18.1.tar.gz && \
 cd gdbm-1.18.1 && \
 CFLAGS="${CFLAGS} -I${BUILD_DEPS}/include" LDFLAGS="${LDFLAGS} -pie -L${BUILD_DEPS}/lib" ./configure --prefix="${BUILD_DEPS}" --enable-static=yes --enable-shared=no --enable-libgdbm-compat && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/gdbm*

# static gpm 1.20.7
ADD patch/gpm.patch /tmp/gpm.patch
RUN cd /tmp && \
 wget https://github.com/telmich/gpm/archive/1.20.7.tar.gz && \
 tar -xvzf 1.20.7.tar.gz && \
 cd gpm-1.20.7 && \
 patch -Np1 -i "/tmp/gpm.patch" && \
 ./autogen.sh && \
 ./configure --prefix=${BUILD_DEPS} --enable-static=yes --enable-shared=no && \
 make -j"${NTHREADS}" && \
 touch doc/gpm.info && \
 make install && \
 rm -Rf /tmp/gpm* && \
 rm -f /tmp/1.20.7* && \
 rm -f "${BUILD_DEPS}/lib/libgpm.so"*

# static gettext 0.20.0
RUN cd /tmp && \
 wget https://ftp.gnu.org/gnu/gettext/gettext-0.20.tar.xz && \
 tar -xvJf gettext-0.20.tar.xz && \
 cd gettext-0.20 && \
 ./configure --prefix="${BUILD_DEPS}" --enable-static=yes --enable-shared=no && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/gettext*

# static libtirpc 1.2.5
RUN cd /tmp && \
 wget https://sourceforge.net/projects/libtirpc/files/libtirpc/1.2.5/libtirpc-1.2.5.tar.bz2 && \
 tar -xvjf libtirpc-1.2.5.tar.bz2 && \
 cd libtirpc-1.2.5 && \
 ./configure --prefix="${BUILD_DEPS}" --enable-static=on --enable-shared=off --disable-gssapi && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/libtirpc*

# static libnsl 1.2.0
RUN cd /tmp && \
wget https://github.com/thkukuk/libnsl/archive/v1.2.0.tar.gz && \
tar -xvzf v1.2.0.tar.gz && \
cd libnsl-1.2.0 && \
./autogen.sh && \
sed 's/0.19/0.20/g' -i po/Makefile.in.in && \
./configure --prefix="${BUILD_DEPS}" --enable-static=on --enable-shared=off && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -f /tmp/v1.2.0* && \
 rm -Rf /tmp/libnsl*

# static sqlite 3.31.1
RUN cd /tmp && \
 wget https://www.sqlite.org/2020/sqlite-autoconf-3310100.tar.gz && \
 tar -xvzf sqlite-autoconf-3310100.tar.gz && \
 cd sqlite-autoconf-3310100 && \
 ./configure --prefix="${BUILD_DEPS}" --disable-shared --enable-static --enable-readline --enable-dynamic-extensions --enable-fts3 && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/sqlite*

# static libdb 4.8
RUN cd /tmp && \
 wget https://download.oracle.com/berkeley-db/db-4.8.30.tar.gz && \
 tar -xvzf db-4.8.30.tar.gz && \
 cd db-4.8.30 && \
 sed -i 's/__atomic_compare_exchange((p), (o), (n))/__atomic_compare_exchange_db((p), (o), (n))/g' dbinc/atomic.h && \
 sed -i 's/static inline int __atomic_compare_exchange(/static inline int __atomic_compare_exchange_db(/g' dbinc/atomic.h && \
 cd build_unix && \
 ../dist/configure --prefix="${BUILD_DEPS}" --disable-shared --enable-static --enable-cxx --enable-compat185 && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/db*

# python 2.7 and shared libpython2.7.so (static libpython2.7.a breaks python modules build with distutils)
ADD patch/alpine/python.patch /tmp/python.patch
RUN cd /tmp && \
 wget https://www.python.org/ftp/python/2.7.17/Python-2.7.17.tar.xz && \
 tar -xvJf Python-2.7.17.tar.xz && \
 cd Python-2.7.17 && \
 envsubst < "/tmp/python.patch" > "python.patch" && \
 patch -p0 -i "python.patch" && \
 CFLAGS="${CFLAGS} -I${BUILD_DEPS}/include -I${BUILD_DEPS}/include/tirpc -I${BUILD_DEPS}/include/ncurses" CPPFLAGS="-I${BUILD_DEPS}/include" LDFLAGS="-L${BUILD_DEPS}/lib" ./configure --prefix="${HYSOP_DEPS}" --enable-shared --with-system-expat --with-system-ffi --enable-unicode=ucs4 --enable-ipv6 --with-ensurepip=yes --enable-optimizations && \
 make -j"${NTHREADS}" && \
 patch -R -p0 -i "/tmp/python.patch" && \
 make install && \
 rm -f /tmp/python.patch && \
 rm -Rf /tmp/Python*

# install and/or update basic python build tools
RUN pip2.7 install --upgrade -vv pip && \
    pip2.7 install --upgrade -vv cython setuptools wheel python-config pytest pybind11


#####################
# LLVM-PROJECT 8.0.1
#####################
RUN cd /tmp && \
 git clone https://github.com/llvm/llvm-project && \
 cd llvm-project && \
 git checkout  llvmorg-8.0.1 && \
 sed -i s/'set(LIBXML2_LIBS "xml2")'/'set(LIBXML2_LIBS "${LIBXML2_LIBRARIES}")'/g llvm/cmake/config-ix.cmake && \
 sed -i 's/llvm::Twine(Z3_get_error_msg(Context, Error))/std::string("")/g' clang/lib/StaticAnalyzer/Core/Z3ConstraintManager.cpp && \
 mkdir build && \
 cd build && \
 cmake -DLIBXML2_LIBRARIES="$(xml2-config --libs)" -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS=-DLLVM_ENABLE_DUMP -DCMAKE_CXX_FLAGS=-DLLVM_ENABLE_DUMP -DLLVM_ENABLE_ASSERTIONS=ON -DLLVM_ENABLE_PROJECTS="clang" -DLLVM_TARGETS_TO_BUILD="X86" -DLLVM_ENABLE_RTTI=ON -DLLVM_ENABLE_EH=ON -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX="${LLVM_ROOT}" ../llvm && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/llvm-project
    
ENV LLVM_CONFIG="${LLVM_ROOT}/bin/llvm-config" 

#######################
# OPENMPI DEPENDENCIES
#######################
# static xz 5.2.4
RUN cd /tmp && \
 wget https://tukaani.org/xz/xz-5.2.4.tar.xz && \
 tar -xvJf xz-5.2.4.tar.xz && \
 cd xz-5.2.4 && \
 ./configure --enable-static --disable-shared --prefix="${BUILD_DEPS}" && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/xz*

# static xml2 2.9.10
RUN cd /tmp && \
 wget ftp://xmlsoft.org/libxml2/libxml2-2.9.10.tar.gz && \
 tar -xvzf libxml2-2.9.10.tar.gz && \
 cd libxml2-2.9.10 && \
 ./configure --enable-static --disable-shared --prefix="${BUILD_DEPS}" && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/libxml*

# shared openmpi 3.1.5 
RUN cd /tmp && \
 wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.5.tar.bz2 && \
 tar -xvkf openmpi-3.1.5.tar.bz2 && \
 cd openmpi-3.1.5 && \
 ./configure --enable-shared --disable-static --with-threads=posix --enable-ipv6 --prefix="${MPI_ROOT}" && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/openmpi*

ENV MPICC="${MPI_ROOT}/bin/mpicc" 
ENV OMPI_MPICC="$CC"
ENV OMPI_MPICXX="$CXX"
ENV OMPI_FC="$FC"

##############
# HDF5 1.10.6
##############
RUN cd /tmp && \
 wget https://github.com/live-clones/hdf5/archive/hdf5-1_10_6.tar.gz && \
 tar -xvzf hdf5-1_10_6.tar.gz && \
 cd hdf5-hdf5-1_10_6 && \
 ./autogen.sh && \
 CC="${MPICC}" LDFLAGS="-Wl,--allow-multiple-definition" ./configure --prefix="${HYSOP_DEPS}" --enable-parallel --enable-shared=yes --enable-static=no && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/hdf5-*


#############
# FFTW 3.3.8
#############
RUN cd /tmp && \
 wget http://www.fftw.org/fftw-3.3.8.tar.gz && \
 tar -xvzf fftw-3.3.8.tar.gz && \
 cd fftw-3.3.8 && \
 CC="${MPICC}" CFLAGS="-I${BUILD_DEPS}/include" LDFLAGS="-L${BUILD_DEPS}/lib" LIBS="-Wl,--start-group -lopen-pal -lopen-rte -lm -lz -Wl,--end-group" ./configure --enable-openmp --enable-threads --enable-mpi --enable-static --disable-shared --with-pic --prefix="${BUILD_DEPS}" && \
 make -j"${NTHREADS}" && \
 make install && \
 CC="${MPICC}" CFLAGS="-I${BUILD_DEPS}/include" LDFLAGS="-L${BUILD_DEPS}/lib" LIBS="-Wl,--start-group -lopen-pal -lopen-rte -lm -lz -Wl,--end-group" ./configure --enable-openmp --enable-threads --enable-mpi --enable-static --disable-shared --with-pic --prefix="${BUILD_DEPS}" --enable-single && \
 make -j"${NTHREADS}" && \
 make install && \
 CC="${MPICC}" CFLAGS="-I${BUILD_DEPS}/include" LDFLAGS="-L${BUILD_DEPS}/lib" LIBS="-Wl,--start-group -lopen-pal -lopen-rte -lm -lz -Wl,--end-group" ./configure --enable-openmp --enable-threads --enable-mpi --enable-static --disable-shared --with-pic --prefix="${BUILD_DEPS}" --enable-long-double && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/fftw*

####################
# BLIS 0.6.1 (blas)
####################
RUN cd /tmp && \
 git clone https://github.com/flame/blis && \
 cd blis && \
 git checkout 0.6.1 && \
 ./configure --disable-static --enable-shared --prefix="${BLAS_ROOT}" --enable-blas --enable-cblas generic && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/blis*

#######################
# FLAME 5.2.0 (lapack)
#######################
RUN cd /tmp && \
 wget https://github.com/flame/libflame/archive/5.2.0.tar.gz && \
 tar -xvzf *.tar.gz && \
 cd libflame-* && \
 ./bootstrap && \
CFLAGS="${CFLAGS} -I${BLAS_ROOT}/include" LDFLAGS="${LDFLAGS} -L${BLAS_ROOT}/lib" ./configure --prefix="${BLAS_ROOT}" --enable-max-arg-list-hack --enable-lapack2flame --enable-multithreading=pthreads --enable-vector-intrinsics=none --enable-dynamic-build && \
 sed -i 's#$(CC) $(CFLAGS_NOOPT)#$(CC) $(CFLAGS_NOOPT) -fPIC#g' Makefile && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/blis*

###################
## PYTHON MODULES
###################

# numpy and scipy (using static blas and lapack)
RUN NPY_NUM_BUILD_JOBS="${NTHREADS}" BLAS="${BLAS_ROOT}/lib/libblis.so" LAPACK="${BLAS_ROOT}/lib/libflame.so" pip2.7 install -vv --upgrade numpy
RUN NPY_NUM_BUILD_JOBS="${NTHREADS}" BLAS="${BLAS_ROOT}/lib/libblis.so" LAPACK="${BLAS_ROOT}/lib/libflame.so" pip2.7 install -vv --upgrade scipy

# numba
RUN pip2.7 install -vv --upgrade numba llvmlite==0.31.0

# mpi4py
RUN pip2.7 install --upgrade mpi4py

# h5py
RUN CC="${MPICC}" HDF5_DIR="${HYSOP_DEPS}" HDF5_VERSION="1.10.6" HDF5_MPI="ON" pip install --upgrade -vv --no-binary=h5py h5py

# other python modules
RUN pip2.7 install -vv --upgrade sympy psutil py-cpuinfo subprocess32 editdistance portalocker tee ansicolors argparse_color_formatter primefac mako configparser backports.tempfile backports.weakref networkx pyvis

# gmpy2 2.0.2
ADD patch/gmpy2.patch /tmp/gmpy2.patch
RUN cd /tmp && \
 wget https://github.com/aleaxit/gmpy/archive/gmpy2_2_0_2.tar.gz && \
 tar -xvzf gmpy2_2_0_2.tar.gz && \
 cd gmpy-gmpy2_2_0_2 && \
 patch -p0 -i "/tmp/gmpy2.patch" && \
 python2.7 setup.py build && \
 pip2.7 install -vv . && \
 rm -Rf /tmp/gmpy*

# HPTT (CPU tensor permutation library + python bindings)
ADD patch/hptt.patch /tmp/hptt.patch
RUN cd /tmp && \
 git clone https://gitlab.com/keckj/hptt && \
 cd hptt && \
 patch -p0 -i "/tmp/hptt.patch" && \
 mkdir build && \
 cd build && \
 CXXFLAGS="${CXXFLAGS} -fopenmp" LDFLAGS="${LDFLAGS} -fopenmp" cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${HYSOP_DEPS}" .. && \
 make -j"${NTHREADS}" && \
 make install && \
 cd ../pythonAPI && \
 HPTT_ROOT="${HYSOP_DEPS}" pip2.7 install -vv . && \
 rm -Rf /tmp/hptt

# fork of memory_tempfile for python 2.7
RUN cd /tmp && \
 git clone https://gitlab.com/keckj/memory-tempfile && \
 cd memory-tempfile && \
 pip2.7 install . && \
 rm -Rf /tmp/memory-tempfile

# pyfftw (FFTW wrapper with R2R transforms, OpenMP and static FFTW capabilities)
ADD patch/pyfftw.patch /tmp/pyfftw.patch
RUN cd /tmp && \
 git clone https://github.com/drwells/pyFFTW && \
 cd pyFFTW && \
 git checkout r2r-try-two && \
 sed -i 's/(fftw3[fl]?_)threads/1omp/g' setup.py && \
 patch -p0 -i "/tmp/pyfftw.patch" && \
 STATIC_FFTW_DIR="${BUILD_DEPS}/lib" CFLAGS="${CFLAGS} -Wl,-Bsymbolic -I${BUILD_DEPS}/include -fopenmp" python2.7 setup.py build_ext --inplace && \
 pip2.7 install . && \
 rm -f /tmp/pyfftw.patch && \
 rm -Rf /tmp/pyFFTW


# OPENCL

################
## OPENCL 1.2 ##
################

# headers
RUN cd /tmp && \
 git clone https://github.com/KhronosGroup/OpenCL-Headers && \
 cd OpenCL-Headers && \
 mv CL "${OPENCL_ROOT}/include" && \
 rm -Rf /tmp/OpenCL-Headers

# ocl-icd ICD loader (Khronos ICD loader does not seem to work well with POCL)
#OCL_ICD_VENDORS
RUN cd /tmp && \
 git clone https://github.com/OCL-dev/ocl-icd && \
 cd ocl-icd && \
 ./bootstrap && \
 CFLAGS="${CFLAGS} -I${OPENCL_ROOT}/include -O2" ./configure --prefix="${OPENCL_ROOT}" --enable-custom-vendordir && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/ocl-icd

# clpeak
ADD cmake/FindOpenCL.cmake /tmp/FindOpenCL.cmake
RUN cd /tmp && \
 git clone https://github.com/krrishnarraj/clpeak && \
 cd clpeak/ && \
 mv /tmp/FindOpenCL.cmake cmake/ && \
 sed -i 's#include(BuildIcd)##g' CMakeLists.txt && \
 mkdir build && \
 cd build/ && \
 CFLAGS="${CFLAGS} -I${OPENCL_ROOT}/include" LDFLAGS="${LDFLAGS} -L${OPENCL_ROOT}/lib" cmake .. && \
 make -j"${NTHREADS}" && \
 mv clpeak "${OPENCL_ROOT}/bin" && \
 cd - && \
 rm -Rf /tmp/clpeak

# clinfo
RUN cd /tmp && \
 git clone https://github.com/Oblomov/clinfo && \
 cd clinfo && \
 CFLAGS="${CFLAGS} -I${OPENCL_ROOT}/include" LDFLAGS="${LDFLAGS} -L${OPENCL_ROOT}/lib" make && \
 mv clinfo "${OPENCL_ROOT}/bin" && \
 rm -Rf /tmp/clinfo

# clfft
RUN cd /tmp && \
 git clone https://github.com/clMathLibraries/clFFT && \
 cd clFFT && \
 cd src && \
 mkdir build && \
 cd build && \
 LDFLAGS="-L${GCC_ROOT}/lib -lstdc++" cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${OPENCL_ROOT}" .. && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/clFFT

# Portable OpenCL (POCL)
RUN cd /tmp && \
 wget https://github.com/pocl/pocl/archive/v1.5.tar.gz && \
 tar -xvzf v1.5.tar.gz && \
 cd pocl-1.5 && \
 mkdir build && \
 cd build && \
 CFLAGS="${CFLAGS} -I${OPENCL_ROOT}/include" CXXFLAGS="${CXXFLAGS} -I${OPENCL_ROOT}/include" LDFLAGS="${LDFLAGS} -L${OPENCL_ROOT}/lib -lOpenCL" cmake -DCMAKE_INSTALL_PREFIX="${OPENCL_ROOT}" -DCMAKE_BUILD_TYPE=Release -DSINGLE_LLVM_LIB=ON -DENABLE_CUDA=OFF -DLLVM_CONFIG="${LLVM_CONFIG}" -DENABLE_ICD=ON .. && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -f /tmp/v1.5.tar.gz && \
 rm -Rf /tmp/pocl-*

# pyopencl 2019.1.2 (using OpenCL ICD)
RUN cd /tmp && \
 wget https://files.pythonhosted.org/packages/b8/4a/440670555eb5c9a5dccb9869cdc0a7ec2fea2fc17668027a9d8bcef1987d/pyopencl-2019.1.2.tar.gz && \
 tar -xvzf pyopencl-2019.1.2.tar.gz && \
 cd pyopencl-2019.1.2 && \
 python2.7 configure.py --cl-inc-dir="${OPENCL_ROOT}/include" --cl-lib-dir="${OPENCL_ROOT}/lib" && \
 make -j"${NTHREADS}" && \
 pip2.7 install -vv . && \
 rm -Rf /tmp/pyopencl*

# gpyFFT (clFFT python wrapper)
RUN cd /tmp && \
 git clone https://github.com/geggo/gpyfft && \
 cd gpyfft && \
 sed -i "s#CL_INCL_DIRS = .*#CL_INCL_DIRS = ['${OPENCL_ROOT}/include']#g" setup.py && \
 sed -i "s#CLFFT_DIR = .*#CLFFT_DIR = '${OPENCL_ROOT}'#g" setup.py && \
 sed -i "s#CLFFT_LIB_DIRS = .*#CLFFT_LIB_DIRS = ['${OPENCL_ROOT}/lib']#g" setup.py && \
 pip2.7 install -vv . && \
 cd - && \
 rm -Rf /tmp/gpyfft

# Oclgrind 19.10
ADD patch/oclgrind.patch /tmp/oclgrind.patch
RUN cd /tmp && \
 wget https://github.com/jrprice/Oclgrind/archive/v19.10.tar.gz && \
 tar -xvzf v19.10.tar.gz && \
 cd Oclgrind-19.10 && \
 patch -p0 -i "/tmp/oclgrind.patch" && \
 mkdir build && \
 cd build && \
 CXXFLAGS="${CFLAGS} -I${BUILD_DEPS}/include" LDFLAGS="${LDFLAGS} -L${BUILD_DEPS}/lib" cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${OPENCL_ROOT}" -DLLVM_DIR="${LLVM_ROOT}/lib/cmake/llvm" .. && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -f /tmp/v19.10.tar.gz && \
 rm -f /tmp/oclgrind.patch && \
 rm -Rf /tmp/Oclgrind-*

ENV LD_LIBRARY_PATH="${OPENCL_ROOT}/lib:${LD_LIBRARY_PATH}"

# install hysop (require authorized host)
ADD hysop /tmp/hysop
RUN cd /tmp/hysop && \
 mkdir build && \
 cd build && \
 cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_POLICY_DEFAULT_CMP0074=NEW -DFFTW_DIR="${BUILD_DEPS}" .. && \
 make -j8 && \
 pip2.7 install -vv . && \
 rm -rf /tmp/hysop

#####################################
# FIX BINARIES AND SHARED LIBRARIES
#####################################

RUN dlkdl

# LIBC MUSL 1.2.0
ENV LIBC_ROOT="${HYSOP_DEPS}/opt/libc"
RUN cd /tmp && \
 wget https://musl.libc.org/releases/musl-1.2.0.tar.gz && \
 tar -xvzf musl-1.2.0.tar.gz && \
 cd musl-1.2.0 && \
 ./configure --prefix="${LIBC_ROOT}" --syslibdir="${HYSOP_DEPS}/lib" --disable-static && \
 make -j"${NTHREADS}" && \
 make install && \
 rm -Rf /tmp/musl-*

# remove all runtime path (rpath)
RUN find "${HYSOP_DEPS}" -type f -executable -exec patchelf --remove-rpath {} \; 2>/dev/null

# replace the dynamic linker of all binaries
RUN find "${HYSOP_DEPS}" -type f -executable -exec patchelf --set-interpreter "${HYSOP_DEPS}/lib/ld-musl-x86_64.so.1" {} \; 2>/dev/null
 
RUN cd /tmp/ && \
 mkdir -p "${HYSOP_DEPS}/etc" && \
 echo "${HYSOP_DEPS}/lib" >> "${HYSOP_DEPS}/etc/ld-musl-x86_64.path" && \
 echo "${GCC_ROOT}/lib64" >> "${HYSOP_DEPS}/etc/ld-musl-x86_64.path" && \
 echo "${LLVM_ROOT}/lib" >> "${HYSOP_DEPS}/etc/ld-musl-x86_64.path" && \
 echo "${BLAS_ROOT}/lib" >> "${HYSOP_DEPS}/etc/ld-musl-x86_64.path" && \
 echo "${MPI_ROOT}/lib" >> "${HYSOP_DEPS}/etc/ld-musl-x86_64.path" && \
 echo "${OPENCL_ROOT}/lib" >> "${HYSOP_DEPS}/etc/ld-musl-x86_64.path"

# Clean image
RUN rm -rf "${HOME}/.cache/pip/"*
RUN rm -Rf /tmp/*

ENV LD_LIBRARY_PATH=
ENV PATH="${HYSOP_DEPS}/bin:${OPENCL_ROOT}/bin:${PATH}"
RUN ln -s "${HYSOP_DEPS}/lib/ld-musl-x86_64.so.1" "${HYSOP_DEPS}/bin/ld"

