#!/usr/bin/env bash
set -euf -o pipefail

container_id=$(docker create keckj/hysop_portable:centos6_release)
docker cp "${container_id}:/release" "/tmp/release"
docker rm -f "${container_id}"
