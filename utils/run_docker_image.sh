#!/usr/bin/env bash
set -euf -o pipefail
docker run -it --volume "$HYSOP_ROOT:/hysop" --rm 'keckj/hysop_portable:debian_etch_gcc93_hysopimg'
