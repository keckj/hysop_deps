import numpy as np
import re, urllib2
from bs4 import BeautifulSoup

def extract_version_name(s):
    version = re.compile('\s*([0-9]+(?:\.[0-9]+)*)?\s*(LTS)?\s*(.*)\s*')
    match = version.match(s)
    s0 = '{}{}'.format('' if match.group(1) is None else '{} '.format(match.group(1)),
                       '' if match.group(2) is None else '{}'.format(match.group(2)))
    s1 = '' if match.group(3) is None else '{}'.format(match.group(3)).title()
    return s0, s1

def extract_date(s):
    year = re.compile('\s*([12][0-9]{3}).*')
    match = year.match(s)
    return str(match.group(1)) if match else ''
    

def extract_info(distribution, *names):
    names = ('Feature',) + names
    page = 'https://distrowatch.com/table.php?distribution={}'.format(distribution)
    page = urllib2.urlopen(page)
    soup = BeautifulSoup(page, 'html.parser')
   
    tables = soup.find_all('table')
    for table in tables:

        header = table.findAll('th')
        headers = tuple(th.text for th in header)
        if (not headers) or (headers[0] != 'Feature'):
            continue
        rows = table.findAll('tr')
        
        columns = []
        for name in names:
            match = filter(lambda x: name in x, headers)
            if not match:
                raise RuntimeError('Could not find \'{}\'.'.format(name))
            idx = headers.index(match[0])
            if headers[0] == headers[1]:
                idx //= 2
            
            td = rows[idx].findAll('td')
            data = tuple(map(lambda x: str(x.get_text()), td))
            if (name == 'Feature'):
                data = map(extract_version_name, data)
                version = map(lambda x: x[0], data)
                name = map(lambda x: x[1], data)
                columns.append(version)
                #columns.append(name)
            elif (name == 'Release'):
                data = map(extract_date, data)
                columns.append(data)
            else:
                columns.append(data)
        for line in np.asarray(columns).T:
            print('| {} |'.format(' | '.join(line)))

if __name__ == "__main__":
    distributions = ['kdeneon']
    for distribution in distributions:
        extract_info(distribution, 'Release', 'gcc', 'libc', 'linux')
