# This script is deprecated. 
# See Dockerfile and patch/python.patch for actual workaround. 
#
# Setuptools build are broken so we relink all dynamic libraries with 
# our static libraries untill there is no more undefined symbols.
# Circular dependencies are handled through the --start-group / --end group
# options of the linker. Note that setting extra_link_args in site.cfg
# will not work because the flags are not appended for every *.so file.
# Setting LDFLAGS globally will also fail because of some configure scripts failing.
# Also note that LDFLAGS != LDLIBS, LDFLAGS come before the objects and LDLIBS after.

# This script builds packages the normal way by using the provided site.cfg.
# It then proceeds to parse the build logs to find info about *.so linking.
# We detect '-shared' parameters and simply replace '-o' with '${LDLIBS} -o'.
# We then provide a custom LDLIBS to this newly generated script and
# we enforce that all symbols are present with -Wl,--no-undefined.
# Finally we check the validity of elf binaries (executables and *.so)
# before installing the package with pip.

# initial build
python2.7 setup.py clean --all build 2>&1 | tee build_logs

# generate script
echo '#!/usr/bin/env bash' > fix_broken_build.sh
echo 'set -eufx -o pipefail' >> fix_broken_build.sh
echo '' >> fix_broken_build.sh
cat build_logs | grep '\-shared' | sed 's/-o/${LDLIBS} -o/g' | awk '{print $0,"\n"}' >> fix_broken_build.sh

# execute script
chmod +x fix_broken_build.sh
export STATIC_LIBS=$(find "${HYSOP_DEPS}/lib" -type f -name '*.a')
export LDLIBS="-Wl,--no-undefined -Wl,--start-group ${STATIC_LIBS} -Wl,--end-group"
./fix_broken_build.sh

# check that there is no missing symbols
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
${SCRIPT_DIR}/check_elf_binaries.sh "$(pwd)" "1"

# install package
pip2.7 install .

exit $?
