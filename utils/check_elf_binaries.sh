#!/usr/bin/env bash
if [[ ! -z "$2" ]]; then
    echo "Checking ELF executables in directory '$1'..."
fi
if [[ ! -d "$1" ]]; then
    echo "Error: '$1' is not a directory."
    exit 2
fi

CANDIDATES=$(find "$1" -type f -executable -exec head -c 4 {} \; -exec echo " {}" \; | grep '^.ELF' 2>/dev/null | awk '{print $2}')
for candidate in ${CANDIDATES}; do
    ldd "${candidate}" >/dev/null 2>&1
    if [[ "$?" -ne "0" ]]; then
        >&2 echo " >ELF executable '${candidate}' seems to be broken, aborting"
        exit 1
    elif [[ ! -z "$2" ]]; then
        echo " >ELF executable '${candidate}' seems to work correctly."
    fi
done
exit $?
