#! /usr/bin/env bash
set -fu -o pipefail

declare -A VERSIONS

function extract_highest_symbol_version() {
    local FILE MAX_VERSION ALL_VERSIONS DEPENDENCIES DEPENDENCY_MAX_VERSION DEPENDENCY
    if [[ ${#} -ne 2 ]]; then
        echo "The function extract_highest_symbol_version() takes exactly two arguments."
        return 1
    fi
    if [[ ! -f "${1}" ]]; then
        echo "File '${1}' does not exist."
        return 1
    fi
    if [[ -z "${2}" ]]; then
        echo "Symbol '${2}' cannot be set to an empty string."
        return 1
    fi

    FILE=$(realpath "${1}")
    ALL_VERSIONS="$(objdump -T "${FILE}" | grep -hoe "${2}[0-9]\+\.[0-9]\+\(\.[0-9]\+\)\?" | sed "s/${2}//" | sort -Vu | tail -1)"
    DEPENDENCIES="$(lddtree "${FILE}" 2>/dev/null | tail -n +2 | grep -o '=> .*$' | sed 's/=> //' | sort -u | xargs)"
    for DEPENDENCY in ${DEPENDENCIES}; do
        DEPENDENCY="$(realpath "${DEPENDENCY}")"
        case "$(basename -- ${DEPENDENCY})" in 
            ld-*.so* )         continue;;
            libc-*.so* )       continue;;
            libm-*.so* )       continue;;
            libdl-*.so* )      continue;;
            librt-*.so* )      continue;;
            libbsd*.so* )      continue;;
            libutil-*.so* )    continue;;
            libpthread-*.so* ) continue;;
        esac
        if [[ ! -f "${DEPENDENCY}" ]]; then
            continue
        fi
        if [ -v "VERSIONS["${DEPENDENCY}"]" ]; then
            DEPENDENCY_MAX_VERSION="${VERSIONS["${DEPENDENCY}"]}"
        else
            DEPENDENCY_MAX_VERSION="$(objdump -T "${DEPENDENCY}" | grep -hoe "${2}[0-9]\+\.[0-9]\+\(\.[0-9]\+\)\?" | sed "s/${2}//" | sort -Vu | tail -1)"
            VERSIONS["${DEPENDENCY}"]="${DEPENDENCY_MAX_VERSION}"
        fi
        ALL_VERSIONS="${ALL_VERSIONS} ${DEPENDENCY_MAX_VERSION}"
    done

    MAX_VERSION="$(echo "${ALL_VERSIONS}" | xargs -n1 | sort -V | tail -1)"
    VERSIONS["${FILE}"]="${MAX_VERSION}"
    echo "${MAX_VERSION}"
    return 0
} 

for file in $@; do
    extract_highest_symbol_version "${file}" 'GLIBC_' >/dev/null
    FILE=$(realpath "${file}")
    if [ -v "VERSIONS["${FILE}"]" ]; then
        echo "${file} => ${VERSIONS["${FILE}"]}"
    else
        echo "${file} => no dependency"
    fi;
done;

#echo
#echo "FULL DEPENDENCY REPORT:"
#for key in $(echo "${!VERSIONS[@]}" | xargs -n1 | sort -u); do
    #value="${VERSIONS["${key}"]}"
    #echo "${key} => ${value}"
#done;
exit 0
