
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Runtime (see https://unix.stackexchange.com/questions/26047/how-to-correctly-add-a-path-to-path, sancho.s 5 jan. 2018)
export PATH="${SCRIPT_DIR}/bin${PATH:+:${PATH}}"
export LD_LIBRARY_PATH="${SCRIPT_DIR}/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"

# Build
export CPATH="${SCRIPT_DIR}/include${CPATH:+:${CPATH}}"
export LIBRARY_PATH="${SCRIPT_DIR}/lib/gcc/x86_64-pc-linux-gnu/10.2.0:${SCRIPT_DIR}/lib${LIBRARY_PATH:+:${LIBRARY_PATH}}"
export CC="${SCRIPT_DIR}/bin/gcc"
export CXX="${SCRIPT_DIR}/bin/g++"
export FC="${SCRIPT_DIR}/bin/gfortran"

# MPI
export MPI_ROOT="${SCRIPT_DIR}"
export MPICC="${MPI_ROOT}/bin/mpicc"
export MPICXX="${MPI_ROOT}/bin/mpicxx"
export OPAL_PREFIX="${MPI_ROOT}"
export OMPI_MPICC="${CC}"
export OMPI_MPICXX="${CXX}"
export OMPI_FC="${FC}"

# OpenCL
export OPENCL_ROOT="${SCRIPT_DIR}"
export OCL_ICD_VENDORS="${SCRIPT_DIR}/etc/OpenCL/vendors"

# Other libraries
export FFTW_ROOT="${SCRIPT_DIR}"
export HPTT_ROOT="${SCRIPT_DIR}"

# Python
export PYTHON_EXECUTABLE="${SCRIPT_DIR}/bin/python3.8 -I"

