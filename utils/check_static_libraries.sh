#!/usr/bin/env bash
if [[ ! -z "$2" ]]; then
    echo "Checking static libraries in directory '$1'..."
fi
if [[ ! -d "$1" ]]; then
    echo "Error: '$1' is not a directory."
    exit 1
fi

CANDIDATES=$(find "$1" -type f -name '*.a' 2>/dev/null)
for candidate in ${CANDIDATES}; do
    SYMBOLS="$(nm ${candidate} | sort -u | grep '_GLOBAL_OFFSET_TABLE_' | tr -d '[:space:]')"
    if [[ "$?" -ne "0" ]]; then
        >&2 echo " >Failed to analyze '${candidate}', aborting"
        exit 2
    fi
    if [[ "${SYMBOLS}" != 'U_GLOBAL_OFFSET_TABLE_' ]]; then 
        >&2 echo " >Static library '${candidate}' has not been compiled with -fPIC."
        exit 1
    elif [[ ! -z "$2" ]]; then
        echo " >Static library '${candidate}' has been compiled with -fPIC."
    fi
done
exit $?
