#!/usr/bin/env bash
set -euf -o pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

TARGET_IMAGE=${1:-centos6}
TARGET_COMMIT=${2:-46-add-python3-x-support}

# init or update git submodules
${SCRIPT_DIR}/init_repository.sh

# select the branch you want to build
cd hysop
git fetch
git checkout "${TARGET_COMMIT}"
cd -

if [[ "${TARGET_IMAGE}" == 'centos6' ]]; then
    INTERMEDIATE_TARGETS='base build deps release'
elif [[ "${TARGET_IMAGE}" == 'alpine' ]]; then
    INTERMEDIATE_TARGETS='hysop'
elif [[ "${TARGET_IMAGE}" == 'debian_etch' ]]; then
    INTERMEDIATE_TARGETS='base build hysop'
else
    echo "Unknown target image '${TARGET_IMAGE}'."
    exit 1
fi
    
# multistage build, we tag each image independantly so that 'docker image prune' 
# do not remove intermediate images.
for target in ${INTERMEDIATE_TARGETS}; do
    echo "${SCRIPT_DIR}/../docker/${TARGET_IMAGE}/Dockerfile"
    docker build --rm=true "${@:3}" --target "${target}" -t "keckj/hysop_portable:${TARGET_IMAGE}_${target}" -f "${SCRIPT_DIR}/../docker/${TARGET_IMAGE}/Dockerfile" ${SCRIPT_DIR}/..
done
exit 0
